## Modern IE vagrant

Based on [markhuber/modern-ie-vagrant](https://github.com/markhuber/modern-ie-vagrant)'s project which is a vagrant box collection for easier testing frontend against different versions of Internet Explorer.
Though the base project offers older versions then IE9, due the less of usage stats I have removed them from availabe boxes.
__NOTE__ The images are downloaded from other [source](http://vagrantbox.es)

## Prerequisites

modern-ie-vagrant requires [Oracle Virtualbox](https://www.virtualbox.org/) and [Vagrant](http://vagrantup.com) be installed and in your path.

## Usage

Checkout the repo [https://gitlab.com/dannystyleart/modern-ie-vagrant.git](https://gitlab.com/dannystyleart/modern-ie-vagrant.git)

To list the available virtual machines

```bash
vagrant status
```
Should result something like
```bash
Current machine states:

IE9-Win7                  not created (virtualbox)
IE10-Win8                 not created (virtualbox)
IE10-Win7                 not created (virtualbox)
IE11-Win7                 not created (virtualbox)
IE11-Win8.1               not created (virtualbox)
EDGE-Win10                not created (virtualbox)
```

To start a new virtual machine

```bash
vagrant up IE10-Win7
```

To start multiple virtual machines at once

```bash
vagrant up IE9-Win7 IE10-Win8 IE10-Win7 IE11-Win7 IE11-Win8.1 EDGE-Win10
```

## Available Vagrant Boxes

* IE9-Win7
* IE10-Win8
* IE10-Win7
* IE11-Win7 
* IE11-Win8.1
* EDGE-Win10
